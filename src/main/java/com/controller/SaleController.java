package com.controller;

import com.entity.SaleEntity;
import com.entity.exception.ResponseExceptionBody;
import com.service.SaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SaleController {

    private static HttpStatus statusNotFound;

    static {
        statusNotFound = HttpStatus.NOT_FOUND;;
    }

    private SaleService saleService;

    @Autowired
    public SaleController(SaleService saleService) {
        this.saleService = saleService;
    }

    @RequestMapping(value = "/all/", method = RequestMethod.GET)
    public ResponseEntity<?> getAllClothes(){
        List<SaleEntity> allData = saleService.getAllData();
        if(allData!=null&& !allData.isEmpty()){
            return new ResponseEntity<>(allData, HttpStatus.OK);
        }else {
            return new ResponseEntity<>(new ResponseExceptionBody(statusNotFound.value(),statusNotFound.getReasonPhrase(),
                    "Data was not found"), statusNotFound);
        }
    }

    @RequestMapping(value = "/clothes/{brandName}", method = RequestMethod.GET)
    public ResponseEntity<?> getSaleData(@PathVariable String brandName){
        List<SaleEntity> dataForBrand = saleService.getAllDataForBrand(brandName);
        if(dataForBrand!=null&& !dataForBrand.isEmpty()){
            return new ResponseEntity<>(dataForBrand, HttpStatus.OK);
        }else {
            return new ResponseEntity<>(new ResponseExceptionBody(statusNotFound.value(), statusNotFound.getReasonPhrase(),
                    String.format("Incorrect path variable %s", brandName)), statusNotFound);
        }
    }

    @RequestMapping(value = "/clothes/{brandName}/{category}", method = RequestMethod.GET)
    public ResponseEntity<?> getSaleData(@PathVariable String brandName, @PathVariable String category){
        List<SaleEntity> dataForBrandAndCategory = saleService
                .getAllDataForBrandAndCategory(brandName, category);
        if(dataForBrandAndCategory!=null&& !dataForBrandAndCategory.isEmpty()){
            return new ResponseEntity<>(dataForBrandAndCategory, HttpStatus.OK);
        }else {
            return new ResponseEntity<>(new ResponseExceptionBody(statusNotFound.value(), statusNotFound.getReasonPhrase(),
                    String.format("Incorrect path variable %s or %s", brandName, category)), statusNotFound);
        }
    }

}

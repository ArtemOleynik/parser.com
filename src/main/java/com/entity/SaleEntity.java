package com.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "sale")
@NamedQuery(name = "Data.getdAll", query = "FROM SaleEntity c")
public class SaleEntity implements Serializable{

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "img")
    private String img;

    @Column(name = "clothes_name")
    private String clothesName;

    @Column(name = "old_cost")
    private String oldCost;

    @Column(name = "new_cost")
    private String newCost;

    @Column(name = "url_to_clothes")
    private String urlToClothes;

    @Column(name = "category")
    private String category;

    @Column(name = "brand_name")
    private String brandName;
}

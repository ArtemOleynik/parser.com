package com.entity.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SaleData {
    private String img;
    private String clothesName;
    private String oldCost;
    private String newCost;
    private String urlToClothes;
    private String category;
    private String brandName;
}

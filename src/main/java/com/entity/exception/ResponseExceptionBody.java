package com.entity.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Artem on 09.04.2019.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseExceptionBody {
    private int statusCode;
    private String status;
    private String reason;
}

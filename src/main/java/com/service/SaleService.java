package com.service;

import com.dao.SaleDao;
import com.entity.SaleEntity;
import com.entity.data.SaleData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SaleService implements SaleDao {

    private SaleDao saleDaoImpl;

    @Autowired
    public SaleService(SaleDao saleDaoImpl) {
        this.saleDaoImpl = saleDaoImpl;
    }

    @Override
    public List<SaleEntity> getAllData() {
        return saleDaoImpl.getAllData();
    }

    @Override
    public List<SaleEntity> getAllDataForBrand(String brandName) {
        return saleDaoImpl.getAllDataForBrand(brandName);
    }

    @Override
    public List<SaleEntity> getAllDataForBrandAndCategory(String brandName, String category) {
        return saleDaoImpl.getAllDataForBrandAndCategory(brandName, category);
    }

    @Override
    public List<SaleData> getData() {
        return saleDaoImpl.getData();
    }

    @Override
    public SaleEntity getSaleEntity(SaleEntity saleEntity) {
        return null;
    }

    @Override
    public void update(SaleEntity saleEntity) {

    }

    @Override
    public void add(SaleEntity saleEntity) {

    }

    @Override
    public void remove(SaleEntity saleEntity) {

    }

    @Override
    public void removeAll() {

    }
}

package com.dao.impl;

import com.dao.SaleDao;
import com.entity.SaleEntity;
import com.entity.data.SaleData;
import com.mysql.cj.core.exceptions.DataReadException;
import org.hibernate.annotations.BatchSize;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class SaleDaoImpl implements SaleDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    @BatchSize(size = 5)
    public List<SaleEntity> getAllData() {
        return entityManager.createNamedQuery("Data.getdAll", SaleEntity.class).getResultList();
    }

    @Override
    @Transactional
    @BatchSize(size = 5)
    public List<SaleEntity> getAllDataForBrand(String brandName) {
        return entityManager.createQuery("SELECT s FROM SaleEntity s where s.brandName=:brandName", SaleEntity.class)
                .setParameter("brandName", brandName).getResultList();
    }

    @Override
    @Transactional
    @BatchSize(size = 5)
    public List<SaleEntity> getAllDataForBrandAndCategory(String brandName, String category) {
        return entityManager.createQuery("SELECT s FROM SaleEntity s where s.brandName=:brandName and s.category=:category", SaleEntity.class)
                .setParameter("brandName", brandName)
                .setParameter("category", category)
                .getResultList();
    }

    @Override
    @Transactional
    public List<SaleData> getData() {
        return entityManager.createQuery(
                "SELECT s.img,s.clothesName,s.newCost,s.oldCost, s.brandName FROM SaleEntity s").getResultList();
    }

    @Override
    @Transactional
    public SaleEntity getSaleEntity(SaleEntity saleEntity) {
        return getAllData().stream().filter(data ->
                data.equals(saleEntity)).findFirst().orElseThrow(() ->
                new DataReadException("DB is not has information!"));
    }

    @Override
    @Transactional
    public void update(SaleEntity saleEntity) {
        entityManager.merge(saleEntity);
    }

    @Override
    @Transactional
    public void add(SaleEntity saleEntity) {
        entityManager.persist(saleEntity);
    }

    @Override
    @Transactional
    public void remove(SaleEntity saleEntity) {
        entityManager.remove(saleEntity);
    }

    @Override
    @Transactional
    public void removeAll() {
        getAllData().forEach(data -> entityManager.remove(data));
    }
}

package com.dao;

import com.entity.SaleEntity;
import com.entity.data.SaleData;

import java.util.List;

public interface SaleDao {
    List<SaleEntity> getAllData();
    List<SaleEntity> getAllDataForBrand(String brandName);
    List<SaleEntity> getAllDataForBrandAndCategory(String brandName, String category);
    List<SaleData> getData();
    SaleEntity getSaleEntity(SaleEntity saleEntity);
    void update(SaleEntity saleEntity);
    void add(SaleEntity saleEntity);
    void remove(SaleEntity saleEntity);
    void removeAll();
}

package com.test.hm.pageobject;

import com.entity.SaleEntity;
import com.test.businesslogic.db.DataBaseBl;
import com.test.utils.fileutils.SelectorCss;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class Pages {

    private SelectorCss selectorCss = SelectorCss.getInstance();

    @Autowired
    private DataBaseBl dataBaseBlImpl;

    public void saveHmData(Document document, String category) {
        Elements elements = document.select(selectorCss.productsHm);
        elements.forEach(element ->
                dataBaseBlImpl.writeToDB(SaleEntity.builder()
                        .brandName("H&M")
                        .category(category)
                        .img(element.select(selectorCss.imgHm).attr("data-src"))
                        .urlToClothes("https://www2.hm.com"+element.select(selectorCss.urlToClothesHm).attr("href"))
                        .clothesName(element.select(selectorCss.clothesNameHm).text())
                        .newCost(element.select(selectorCss.newCostHm).text())
                        .oldCost(element.select(selectorCss.oldCostHm).text()).build())
        );
    }

    public void saveGapData(Document document, String category) {
        String categoryFinal = category.equals("girls")||category.equals("boys") ? "kids" : category;
        Elements elements = document.getElementsByTag("script").select(selectorCss.productsGap);
        elements.forEach(element ->
                dataBaseBlImpl.writeToDB(SaleEntity.builder()
                        .brandName("GAP")
                        .category(categoryFinal)
                        .img(element.select(selectorCss.imgGap).attr("src"))
                        .urlToClothes("https://www.gap.com"+element.select(selectorCss.urlToClothesGap).attr("href"))
                        .clothesName(element.select(selectorCss.clothesNameGap).text())
                        .newCost(element.select(selectorCss.newCostGap).text())
                        .oldCost(element.select(selectorCss.oldCostGap).text()).build())
        );
    }
}

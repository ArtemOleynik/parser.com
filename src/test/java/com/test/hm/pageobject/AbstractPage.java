package com.test.hm.pageobject;

import java.io.*;
import java.sql.*;

/**
 * Created by Artem on 24.03.2019.
 */
public class AbstractPage implements Externalizable {

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        try {
            String page = new Page<String>(DriverManager.getConnection("")).getResultQuery("");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {

    }

    static class Page<N> {
        private Connection con;
        private Statement statement = con.createStatement();

        Page(Connection con) throws SQLException {
            this.con = con;
        }

        N getResultQuery(final String query) {
            N n = null;
            try (ResultSet rs = statement.executeQuery(query)) {
                n = (N) rs.getObject(1);
                statement.close();
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
                try {
                    if (statement != null && !statement.isClosed()) {
                        statement.close();
                    }
                    if (con != null && !con.isClosed()) {
                        con.close();
                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            return n;
        }
    }
}

package com.test.story;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.javascript.JavaScriptEngine;
import com.test.businesslogic.db.DataBaseBl;
import com.test.hm.pageobject.Pages;
import com.test.utils.fileutils.FileData;
import com.test.utils.fileutils.FilesTestData;
import com.test.utils.fileutils.SelectorCss;
import org.apache.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.DataNode;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Steps {

    private SelectorCss selectorCss = SelectorCss.getInstance();


    private Logger logger = Logger.getLogger(Steps.class);

    private DataBaseBl dataBaseBlImpl;

    private FilesTestData testData;

    private Pages pages;

    @Autowired
    public Steps(DataBaseBl dataBaseBlImpl, FilesTestData testData, Pages pages) {
        this.dataBaseBlImpl = dataBaseBlImpl;
        this.testData = testData;
        this.pages = pages;
    }

    //    @PostConstruct
//    public void init(){
//        dataBaseBlImpl.clearDB();
//    }

    @Test
    public void sales() {
//        this.init();
        try {
//            Thread ladiesHm = new Thread(() -> saleHM("ladies"));
//            Thread menHm = new Thread(() -> saleHM("men"));
//            Thread kidsHm = new Thread(() -> saleHM("kids"));

            Thread womenGap = new Thread(() -> saleGap("women's"));
//            Thread menGap = new Thread(() -> saleGap("mens"));
//            Thread kidsGap = new Thread(() -> {
//                saleGap("girls");
//                saleGap("boys");
//            });

//            ladiesHm.start();
//            menHm.start();
//            kidsHm.start();

            womenGap.start();
//            menGap.start();
//            kidsGap.start();

//            ladiesHm.join();
//            menHm.join();
//            kidsHm.join();

            womenGap.join();
//            menGap.join();
//            kidsGap.join();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void saleHM(String category) {
        Document document = null;
        try {
            String hmSale = String.format(testData.getFileData(FileData.HM_SALE.getValue()), category, "500");
            logger.info(hmSale);
            Connection connection = Jsoup.connect(hmSale);
            while (connection.get().isBlock()){
                document = connection.get();
            }
            if (document!=null) {
                pages.saveHmData(document, category);
            }else {
                TimeUnit.SECONDS.sleep(2);
                pages.saveHmData(Jsoup.connect(hmSale).get(), category);
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void saleGap(String category) {
        String userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36";
        try {
            String gapSale = testData.getFileData(FileData.GAP_SALE.getValue());
            Connection connection = Jsoup.connect(gapSale).userAgent(userAgent);
            Document document = connection.get();
            if (document != null) {
                Elements elements = document.select(selectorCss.saleUrls);
                Element url = elements.stream().filter(element -> element.text().contains(category)).findFirst().get();
                String sUrl = url.attr("href");
                logger.info(sUrl);
                Connection con = Jsoup.connect(sUrl).userAgent(userAgent).timeout(5000);
                Document doc = con.get();
                Elements elements1 = doc.getElementsByTag("script");
                WebClient client = new WebClient(BrowserVersion.CHROME);

                for (Element element : elements1) {
                    String text = element.attr("src");
                    if (element.toString().contains("src")) {
                        client.setJavaScriptEngine(new JavaScriptEngine(client));
                        client.getOptions().setJavaScriptEnabled(false);
                        HtmlPage page;
                        if (text.contains("http")){
                            page = client.getPage(text);
                            element.appendText(page.getWebResponse().getContentAsString());
                        }else if (!text.contains(" ")&&!text.contains("") ){
                            page = client.getPage("https://www.gap.com" + text);
                            element.appendText(page.getWebResponse().getContentAsString());
                        }
                    }
                }


//                Document doc = Jsoup.connect(url.attr("href")).userAgent("Chrome/72.0.3626.119").timeout(20*1000).followRedirects(true).execute().parse();


//                Document doc = Jsoup.parse(myPage.getWebResponse().getContentAsString());

                client.setJavaScriptEngine(new JavaScriptEngine(client));
                client.getOptions().setJavaScriptEnabled(true);
                HtmlPage htmlPage = client.getPage(doc.baseUri());


                Document documentCategory = Jsoup.connect(htmlPage.getWebResponse().getContentAsString()).userAgent(userAgent).get();
                pages.saveGapData(documentCategory, category);
                client.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
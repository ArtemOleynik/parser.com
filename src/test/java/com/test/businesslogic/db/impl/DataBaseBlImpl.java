package com.test.businesslogic.db.impl;

import com.dao.SaleDao;
import com.entity.SaleEntity;
import com.entity.data.SaleData;
import com.test.businesslogic.db.DataBaseBl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataBaseBlImpl implements DataBaseBl<SaleEntity> {

    @Autowired
    private SaleDao saleDaoImpl;

    @Override
    public void writeToDB(SaleEntity data) {
        SaleData hmSaleData = SaleData.builder()
                .img(data.getImg())
                .clothesName(data.getClothesName())
                .newCost(data.getNewCost())
                .oldCost(data.getOldCost())
                .urlToClothes(data.getUrlToClothes())
                .category(data.getCategory())
                .brandName(data.getBrandName())
                .build();

        if (!saleDaoImpl.getData().contains(hmSaleData)) saleDaoImpl.add(data);
    }

    @Override
    public void clearDB() {
        if (!saleDaoImpl.getAllData().isEmpty()) saleDaoImpl.removeAll();
    }
}

package com.test.businesslogic.db;

public interface DataBaseBl<D> {
    void writeToDB(D data);
    void clearDB();
}

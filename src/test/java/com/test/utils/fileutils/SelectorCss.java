package com.test.utils.fileutils;

public class SelectorCss {

    private static SelectorCss instance;
    public String productsHm;
    public String imgHm;
    public String urlToClothesHm;
    public String clothesNameHm;
    public String newCostHm;
    public String oldCostHm;

    public String saleUrls;
    public String productsGap;
    public String imgGap;
    public String urlToClothesGap;
    public String clothesNameGap;
    public String newCostGap;
    public String oldCostGap;

    private SelectorCss(){

        //H&M
        productsHm = ".product-item";
        imgHm = ".image-container imgHm";
        urlToClothesHm = ".image-container a";
        clothesNameHm = ".item-details .item-heading a";
        newCostHm = ".item-price .price.sale";
        oldCostHm = ".item-price .price.regular";

        //GAP
        saleUrls = ".seeMoreProducts a";
        productsGap = ".product-card-grid__inner";
        imgGap = ".product-card__image";
        urlToClothesGap = ".product-card__link";
        clothesNameGap = ".product-card__name";
        newCostGap = ".product-price__highlight";
        oldCostGap = ".product-price__strike";
    }

    public static SelectorCss getInstance() {
        if (instance == null){
            instance = new SelectorCss();
        }
        return instance;
    }
}

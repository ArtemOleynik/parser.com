package com.test.utils.fileutils;

import org.springframework.stereotype.Service;

@Service
public interface FilesTestData {
    String getFileData(String data);
}

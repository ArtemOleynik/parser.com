package com.test.utils.fileutils;

public enum FileData {
    HM_SALE("hmSale"),
    GAP_SALE("gapSalePage");

    private String value;

    FileData(String value) {
        this.value = value;
    }

    public String getValue(){
        return value;
    }
}
